console.log('Hello World')


const getCube = 2**3
console.log(`The cube of 2 is ${getCube}`)

// -------------------------------------------

const address = ["E.Rodriguez Sr. Ave.", "Damayang Lagi", "Quezon City"]
const [streetName, baranggay, city] = address
console.log(`I live at ${streetName}, ${baranggay}, ${city}`)

// --------------------------------------------

const animal = {
	name: "Lolong",
	type: "Saltwater crocodile",
	weight: "1075 kg",
	measurement: "20ft 3in"
}

const {name, type, weight, measurement} = animal
console.log(`${name} was as ${type}. He weighed ${weight} with a measurement of ${measurement}`)

// -----------------------------------------------

const numbers = [1, 2, 3, 4, 5];
numbers.forEach((number) => {
	console.log(number)
	return number
})

// -------------------------------------------

class dog{
	constructor (name,age,breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
const myDog = new dog("Frankie", 5, "miniature dachshund")
console.log(myDog)